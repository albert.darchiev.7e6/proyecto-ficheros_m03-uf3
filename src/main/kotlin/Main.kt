import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Scanner
import kotlin.io.path.*

//        PROJECTE FITXERS
//-------- ALAN DE LA CRUZ --------
//-------- ALBERT DARCHIEV --------

val scanner = Scanner(System.`in`)
var importUsersList = mutableListOf<String>()
fun main() {
    createMissingDirectory()
    var path = Path("src/main/kotlin/Import")
    //--------- PROCÉS 1 ---------
    if (checkDirectoryFiles().isNotEmpty()) {
        //-------- 1.1 ---------
        for (file in checkDirectoryFiles()){
            importUsersList += readFile(file)
            removefile(file)
        }
        //-------- 1.2/3 -------
        appendToData(importUsersList)

        //-------- 2.1 ---------
        println(readLastId())
    }
    //-------- 2.2 ---------
    menu()
}

//procés que comprovi si existeix un o més fitxers de càrrega d’usuaris cada cop que l’aplicació s’inicia
fun checkDirectoryFiles(): MutableList<Path> {
    var path = Path("src/main/kotlin/Import")
    val files : List<Path> = path.listDirectoryEntries()
    var importPathList = mutableListOf<Path>()
    for(item in files)  {
        importPathList.add(item)
    }
    return importPathList
}

fun readFile(path: Path): MutableList<String> {
    val text: String = path.readText()
    val mutableText = text.split(";").toMutableList()
    return mutableText.toMutableList()
}

fun removefile(path: Path){
    path.deleteIfExists()
}

fun appendToData(importUsersList: MutableList<String>) {
    var id = 1
    val file = File("src/main/kotlin/Data/userData.txt")
    file.writeText("")
    for (user in importUsersList){
        val newUser = user.split(",").toMutableList()
        if (newUser[4] == "false" && newUser[5] == "true")
        else {
            newUser[0] = "$id"
            var result = newUser.toString().replace(", ", ";")
            file.appendText(result.subSequence(1, result.length - 1).toString()+"\n")
            id++
        }
    }
}

fun readLastId(): Int {
    val path = Path("src/main/kotlin/Data/userData.txt")
    var usersList = readFile(path).toString().split("\n").toMutableList()
    val lastUserId = usersList[usersList.size-2][0]

    return lastUserId.toString().toInt()
}

fun registerUser() {
    println("CREAR USUARI")
    println("Introdueix el teu nom: ")
    val nameuser = scanner.next()
    println("Introdueix el telefon: ")
    val phonenumber = scanner.nextInt()
    println("Introdueix el correu: ")
    val email = scanner.next()
    val file = File("src/main/kotlin/Data/userData.txt")
    file.appendText("${readLastId()+1}; $nameuser; $phonenumber; $email; true; false\n")
    backup()
    menu()
}

fun modifyUser(){
    val path = Path("src/main/kotlin/Data/userData.txt")
    println("Introdueix la id del usuari que vols modificar : ")
    val searchId = scanner.nextInt()
    println("Que vols modificar? \n1.NOM\n2.TELÉFON\n3.EMAIL")
    var modify = scanner.nextInt()
    println("Introduce el nuevo valor")
    var newValue = scanner.next()

    var oldFile = readFile(path)
    var oldUsers = oldFile.toString().subSequence(1, oldFile.toString().length-1).split("\n").toMutableList()
    oldUsers.removeAt(oldUsers.size-1)
    var newMod = mutableListOf<String>()
    var userIdExists = false
    oldUsers.forEachIndexed { index, user ->
        var word = user.split(",").toMutableList()//CAMBIAR NOMBRE DE LA VARIABLE
        if (user.split(",")[0].trim().toInt() == searchId){
            word[modify] = newValue
            userIdExists = true
        }
        newMod += word.toString().subSequence(1,word.toString().length-1).toString().replace("  ", "")
        importUsersList = newMod
    }
    appendToData(newMod)
    if (!userIdExists) println("NO S'HA TROBAT AQUESTA ID D'USUARI")
    else println("L'USUARI S'HA MODIFICAT CORRECTAMENT")
    menu()
}

fun blockUser(){
    val path = Path("src/main/kotlin/Data/userData.txt")
    println("Introdueix la id del usuari que vols modificar : ")
    val searchId = scanner.nextInt()
    println("Modificar l'estat BLOCKED")
    var oldFile = readFile(path)
    var oldUsers = oldFile.toString().subSequence(1, oldFile.toString().length-1).split("\n").toMutableList()
    oldUsers.removeAt(oldUsers.size-1)
    var newMod = mutableListOf<String>()
    var userIdExists = false
    oldUsers.forEachIndexed { index, user ->
        var word = user.split(",").toMutableList()//CAMBIAR NOMBRE DE LA VARIABLE
        if (user.split(",")[0].trim().toInt() == searchId){
            var currentBoolean = word[5].trim().toBoolean()
            if (currentBoolean)word[5] = "false"
            else word[5] = "true"
            userIdExists = true
        }
        newMod += word.toString().subSequence(1,word.toString().length-1).toString().replace("  ", "")
        importUsersList = newMod
}
    appendToData(newMod)
    if (!userIdExists) println("NO S'HA TROBAT AQUESTA ID D'USUARI")
    else println("L'USUARI S'HA MODIFICAT CORRECTAMENT")
    backup()
    menu()

}

fun backup(){
    val from = Path("src/main/kotlin/Data/userData.txt")
    val current = LocalDate.now()
    val formater = DateTimeFormatter.ofPattern("dd-MM-yyyy")
    val currentDate = current.format(formater)
    val to = Path("src/main/kotlin/Backup/${currentDate}_userData.txt")

    Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING)
}

fun createMissingDirectory(){
    val backupPath = Path("src/main/kotlin/Backup")
    val importPath = Path ("src/main/kotlin/Import")
    if (!importPath.exists()){
        importPath.createDirectory()
        }
    if (!backupPath.exists()){
        backupPath.createDirectory()
    }
}

fun menu() {
    println("1. Crear usuari\n2. Modificar Usuari\n3. Des/Bloquejar usuari\n4. SORTIR")
    var option = scanner.nextInt()
    when (option) {
        1 -> registerUser()
        2 -> modifyUser()
        3 -> blockUser()
        4 -> {
            backup()
            println("EXIT")
        }
    }
}

